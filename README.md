# Vagrant LAMP stack with Wordpress
* Sets up a LAMP stack with Vagrant and Virtualbox.
* It uses Puppet to provision the server.
* Wordpress is installed via Composer along with a few select plugins (Advanced Custom Fields 5.0, Gravity Forms, Simple 301 Redirects + Bulk Uploader, Wordpress SEO by Yoast)

## Requirements
* [VirtualBox](https://www.virtualbox.org)
* [Vagrant](http://vagrantup.com)
* [vagrant-hostmanager](https://github.com/smdahlen/vagrant-hostmanager)
	* `vagrant plugin install vagrant-hostmanager`
	* This plugin will kindly add `highrank.local` to your hosts file for you

## Installation
Clone this repository, and replace `project` with your new project name

    $ git clone https://jeffhighrank@bitbucket.org/highrankdevs/ilawyer-starter-project.git

Wordpress will be automatically installed in the `content` folder, following how Rackspace Cloud Sites public directory is set up

## Usage
Start the VM

	$ cd project
	$ vagrant up

As long as everything went correctly, and hopefully it does, you can now access your shiny new Wordpress project at [http://highrank.local](http://highrank.local)

### Database dump & import
Puppet will automatically try to import the database dump `/database/highrank.sql`.

If you are using the default configuration, when you run `vagrant up` a rather default Wordpress installation will be installed by default, and the default plugins should be activated. You will need to activate any other plugins you add with Composer.

To

## Installed software
* Apache 2
* MySQL 5.5
* PHP 5.4 (with mysql, curl, mcrypt, memcached, gd)
* vim, git, composer
* Wordpress
* Wordpress plugins:
	* Advanced Custom Fields
	* Developer plugin
* WP-CLI (via Composer)
* PHP .dotenv for different environment variables
* NOTE: Does not come with a theme by default, that's up to you, but do commit the theme to the repository

## Default credentials
### MySQL
* Username: root
* Password: vagrant
* Host: localhost
* Port: 3306
* DB Name: highrank

**Note:** Remote MySQL access is enabled by default, so you can access the MySQL database using your favorite MySQL client with the above credentials (and using e.g. *highrank.local* as hostname).