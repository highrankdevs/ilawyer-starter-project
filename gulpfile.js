var gulp       = require('gulp'),
    less       = require('gulp-less'),
//    imagemin   = require('gulp-imagemin'),
//    bower      = require('gulp-bower'),
    notify     = require('gulp-notify'),
    livereload = require('gulp-livereload'),
    autoprefix = require('gulp-autoprefixer'),
    rename     = require('gulp-rename'),
    minifyCSS  = require('gulp-minify-css'),
    uglify     = require('gulp-uglify');

var publicDir = 'content';
var themeDir  = publicDir + '/app/themes/ilawyer';

var paths = {

    scripts: [
//		publicDir + '/app/components/bootstrap/js/affix.js',
//		publicDir + '/app/components/bootstrap/js/alert.js',
//		publicDir + '/app/components/bootstrap/js/button.js',
//		publicDir + '/app/components/bootstrap/js/carousel.js',
//		publicDir + '/app/components/bootstrap/js/collapse.js',
//		publicDir + '/app/components/bootstrap/js/dropdown.js',
//		publicDir + '/app/components/bootstrap/js/modal.js',
//		publicDir + '/app/components/bootstrap/js/popover.js',
//		publicDir + '/app/components/bootstrap/js/scrollspy.js',
//		publicDir + '/app/components/bootstrap/js/tab.js',
//		publicDir + '/app/components/bootstrap/js/tooltip.js',
//		publicDir + '/app/components/bootstrap/js/transition.js',
        themeDir + '/assets/js/src/**/*.js',
    ],

    less: themeDir + '/assets/less/app.less',

    images: [
        themeDir + '/assets/img/**/*'
    ],

    bower: publicDir + '/app/components',

};

var destination = {

    css: themeDir + '/assets/css',
    scripts: themeDir + '/assets/js',
    images: themeDir + '/assets/img',

};

// Losslessly compress images
//gulp.task('images', function() {
//    return gulp.src(paths.images)
//        .pipe(imagemin())
//        .pipe(gulp.dest(destination.images));
//});


gulp.task('less', function () {
    return gulp.src(paths.less)
        .pipe(less()).on('error', function(err){
            console.warn(err.message)
        })
        .pipe(autoprefix('last 5 version'))
        .pipe(rename('./main.min.css'))
        .pipe(minifyCSS())
        .pipe(gulp.dest(destination.css))
        .pipe(livereload());
});

gulp.task('js', function() {
    return gulp.src(paths.scripts)
        .pipe(uglify())
        .pipe(rename('./scripts.min.js'))
        .pipe(gulp.dest(destination.scripts))
        .pipe(livereload());
});

gulp.task('watch', function() {
    var server = livereload();

    gulp.watch(themeDir + '/assets/less/**/*.less', ['less']);
    gulp.watch(themeDir + '/assets/js/src/**/*.js', ['js']);
    gulp.watch(paths.images, ['images']);
    gulp.watch(themeDir + '/**/*.php').on('change', function(file) {
        server.changed(file.path);
    });

});

gulp.task('default', ['less', 'js', 'watch']);