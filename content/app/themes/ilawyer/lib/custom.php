<?php
/**
 * Custom functions
 */

////////// CUSTOM POST TYPES //////////
add_action( 'init', 'create_post_type' );
function create_post_type() {
    ////////// ATTORNEYS //////////
    register_post_type( 'attorney',
        array(
            'labels' => array(
                'name' => __( 'Attorneys' ),
                'singular_name' => __( 'Attorneys' )
            ),
            'public' => true,
            'has_archive' => true,
        )
    );
    ////////// TESTIMONIALS //////////
    register_post_type( 'testimonial',
        array(
            'labels' => array(
                'name' => __( 'Testimonials' ),
                'singular_name' => __( 'Testimonial' )
            ),
            'public' => true,
            'has_archive' => true,
        )
    );
    ////////// PRESS //////////
    register_post_type( 'press',
        array(
            'labels' => array(
                'name' => __( 'Press' ),
                'singular_name' => __( 'Press' )
            ),
            'public' => true,
            'has_archive' => true,
        )
    );
}

////////// OPTIONS PAGES FOR ADVANCED CUSTOM FIELDS //////////
if( function_exists('acf_add_options_page') ) {
    ////////// THEME GENERAL SETTINGS //////////
    acf_add_options_page(array(
        'page_title' 	=> 'Theme General Settings',
        'menu_title'	=> 'Theme Settings',
        'menu_slug' 	=> 'theme-general-settings',
        'capability'	=> 'edit_posts',
        'redirect'		=> false
    ));
    ////////// HEADER SETTINGS //////////
    acf_add_options_sub_page(array(
        'page_title' 	=> 'Theme Header Settings',
        'menu_title'	=> 'Header',
        'parent_slug'	=> 'theme-general-settings',
    ));
    ////////// FOOTER SETTINGS //////////
    acf_add_options_sub_page(array(
        'page_title' 	=> 'Theme Footer Settings',
        'menu_title'	=> 'Footer',
        'parent_slug'	=> 'theme-general-settings',
    ));
}